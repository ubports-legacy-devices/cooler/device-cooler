#!/bin/sh
status="$(cat /sys/class/switch/hdmi/state)"
if [ "${status}" = 1 ]
then
	/system/bin/hdmi_loader hdmi on
elif [ "${status}" = 0 ]
then
	/system/bin/hdmi_loader hdmi off
fi
