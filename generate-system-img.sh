#! /bin/sh

set -e

ROOTFS="$(realpath "$1")"
OUTPUT_IMG="$2"

# The UUID is probably irrelevant, but just for extra safety re-use the one
# from the current released devices:
UUID="7737b09a-291e-a752-a883-f5e38364b952"

cd "$ROOTFS"
setfacl --restore="${ROOTFS}.permissions"
cd -

dd if=/dev/zero of="$OUTPUT_IMG" bs=4K count=62162
mke2fs \
    -O extent,sparse_super,large_file,uninit_bg \
    -E root_owner=0:0 \
    -L SYSTEM \
    -U "$UUID" \
    -m 0 \
    -I 256 -i 16385 \
    -e remount-ro \
    -d "$ROOTFS" \
    "$OUTPUT_IMG"
